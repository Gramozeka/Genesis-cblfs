# CBLFS-SemiAutomatic
This project is based on http://clfs.org/ and http://www.linuxfromscratch.org/blfs/.
It assembles three automated conveyors for self-assembly of the finished multi-architecture system (32 + 64 bit)
with the KDE5 graphic shell, capable of assembling and running applications of both 32-bit and 64-bit architectures.

important!

first you need to read the instructions in the section ReadMe-Now

after all recommendations, go to the section CLFS-x86_64-Multilib-BaseSystem

read the instructions, get the source and after start compilation

and then go to the section CBLFS-x86_64-Multilib, read the instructions, get the source and after start compilation